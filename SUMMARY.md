# Table of contents

* [OpenMediaVault](README.md)

## Installation

* [Installation](installation/install.md)

## Setup

* [Updates](setup/updates.md)
* [Plugins](setup/plugins.md)
* [Users](setup/users.md)
* [Shares](setup/shares.md)
* [OMV-Extras](setup/omv-extras.md)
