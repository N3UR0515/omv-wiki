---
description: >-
  Welcome to another of my guides! This guide covers the installation and
  setup of OpenMediaVault.
cover: .gitbook/assets/omv-background.png
coverY: 0
---

# OpenMediaVault
