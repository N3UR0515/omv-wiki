# Install

To get started with OpenMediaVault, obviously you'll need to install it first.

This guide focuses on the installation of **OpenMediaVault 6.0.24**, on either a **virtual machine** or **bare-metal**.

Alternatively, you could deploy this on an **[8GB Raspberry Pi 4 or 400](https://www.raspberrypi.com/products/)** creating a **powerhouse pocket `NAS`**!

Start by downloading the **[OpenMediaVault 6](https://www.openmediavault.org/?page_id=77) ISO** file to your machine.

To install on **bare-metal**, you'll need a bootable USB drive if not using TFTP. *(You can do this with various tools such as [Rufus](https://rufus.ie/en/) or [Universal USB Installer](https://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/))*.

**Note:** *To install on a **virtual machine** you may need to upload the **Ubuntu Desktop ISO** to your `Hypervisor`, or have a file mount configured to be able to mount the `ISO` for use with the **VM**.*

Once ready, set the machine to boot from the **OpenMediaVault ISO** either directly or from USB.

The install process is pretty straightforward, but watch the short video below for help if needed.

[![OMV Install](../images/omv-background.png)](https://youtu.be/ZMhZQ4fju4I)

***

## Management Interface

To start managing **OMV** you'll need to enter your servers `IP address` into your preferred browser.

You can login to the webpage with the credentials listed below:

- username: `admin`
- password: `openmediavault`

![omv-webpage](./../images/omv-webpage.png)

***

## Password Change

As with any machine you configure, the first thing you should do is login and change the default password!

You can do this by clicking the `gear` button ![gear-icon](./../images/gear-icon.png) in the top right of the screen, and then selecting `Change Password`.

![password-change](./../images/omv-password-change.png)

![password-change-2](./../images/omv-password-change-2.png)

***

## Done

And that's it, OMV is now installed! Good job! 👏
