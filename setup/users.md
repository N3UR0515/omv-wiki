# Users

To create a new user account, navigate to `Users` > `Users` and click the `Create | Import` button ![create-user-icon](./../images/create-shared-folder-icon.png) and choose `Create`.

![create-new-user](./../images/create-new-user.png)

## Username

Enter the `username` you would like for this account, bearing in mind this is `Case-Sensitive`.

**Note:** By default usernames are all lower-case on Linux. Because of this I recommend using `lower-case` usernames in the `Name` field for simplicity. I mention this because if you try and access the server via `SSH` for example with the username `doris`, instead of `Doris` (Capital *D*), it will fail to authenticate.

## Groups

Click the `Groups` dropdown ![select-groups](./../images/select-groups.png) to choose the groups this account will be added to.

Upon clicking the `Groups` dropdown a scrollable table will be appear, as shown below.

![group-selection-field](./../images/group-selection-field.png)

There will be quite a few groups in here, but these are the main ones to look for:

- `sudo`
  - Gives the User admin (sudo) privileges
- `ssh`
  - Allows the User to login to the system via `SSH`
- `openmediavault-admin`
  - Makes the User an OMV Administrator
- `openmediavault-webgui`
  - Allows the User to access OMV from the `management web page`
- `sambashare`
  - Allows the User to access `SMB shares`

**Note:** These will get you started for now, but you should always check back here when installing new packages. Ensure you add your account to the appropriate groups that are created later. For example we will later be installing `Docker`, so we will need to go back to this page and add our account to the `docker` group.

Once you are done simply click the `Save` button ![save-button-icon](./../images/save-button-icon.png) to create the account.
