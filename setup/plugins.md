# Plugins

To add additional features and services to your OpenMediaVault server you'll need to navigate to `System` > `Plugins` in the left menu pane.

![plugins-page](./../images/plugins-page.png)

From here we can select the additional packages we would like to install by clicking the package and then clicking the `Install` Button ![install-plugins-icon](./../images/install-plugin-icon.png) located under the main navigation menu.

![plugins-page](./../images/install-plugins.png)

I recommend installing the following plugins to get started:

- openmediavault-filebrowser
- openmediavault-sharerootfs

Once you have installed [OMV-Extras](./omv-extras.md) though, be sure to come back and install any other packages you need. I recommend installing these `plugins`:

- `openmediavault-apttool`
  - Allows managing your `apt repositories`
- `openmediavault-compose`
  - For use with `Docker` later
- `openmediavault-fail2ban`
  - Protects against suspicious logins
- `openmediavault-kvm`
  - If you want to run `virtual machines` on your server
- `openmediavault-minidlna`
  - For use with serving media files through `Plex` and over the `LAN`
- `openmediavault-remotemount`
  - If you want to mount external filesystems to this server, such as `NAS` share
  - `openmediavault-wetty`
    - If you want to be able to connect to your `OMV` server's terminal in the `management web page`
