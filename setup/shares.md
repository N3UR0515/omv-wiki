# Shares

This section is where we are going to configure the network shares that OpenMediaVault will host on the LAN.

This would be a good time to go and create any other [user accounts](./users.md) you need for accessing your server, if you haven't already.

For example we certainly **do not** want `Doris` logging in to our server with the main `admin` account and breaking things. `Doris` should have her own user account to access the media files with less permissions.

**Note:** Remember to press the yellow `confirm pending changes` prompt at the top of the screen after making changes to each section.

![confirm-pending-changes](./../images/confirm-pending-changes.png)

***

## Filesystem

Before we can create any shares, we first need to setup the `filesystem`.

To do this, navigate to `Storage` > `File Systems` in the left menu pane.

![filesystems-page](./../images/filesystems-page.png)

From here select the `Create|Mount` button ![add-filesystem-icon](./../images/add-filesystem-icon.png) located under the main navigation bar, and then the `Create` button.

![add-filesystem](./../images/add-filesystem.png)

Choose your click the `Device` dropdown and select the device you are going to use to create your shares on (It will appears as something like `/dev/sda2/`).

You can leave the `Filesystem Type` as `EXT4` (Default Linux filesystem)

![added-filesystem](./../images/added-filesystem.png)

***

## Shared Folders

Next head to `Storage` > `Shared Folders` in the left menu pane, and click the `Create` shared folder button ![create-shared-folder-icon](./../images/create-shared-folder-icon.png).

![shared-folders-page](./../images/shared-folders-page.png)

Enter the name of the `share` that will be accessed. For example a `Media` folder, which will contain our `Downloads`, `Movies` and `TV` folders etc for later use.

Select the `filesystem` you created earlier.

I recommend leaving the `Relative path` option as the one that is automagically generated when you enter the `share name`. This folder can then be accessed by changing to the `/export/Media` directory (`cd /export/Media`) when accessing OMV from the terminal.

You can leave the `Permissions` option as the default for now, especially for the `Media` share etc.

You can also just leave the `Comment` field blank.

Now click the `Save` button ![save-button-icon](./../images/save-button-icon.png) to create the `shared folder`.

![created-shared-folder](./../images/created-shared-folder.png)

Continue this process to create any other `shared folders` you need.

**Note:** If you plan to use OMV for use with `Docker`, `KVM` etc then I recommend creating these additional shares:

- ISOs
  - To store ISO images for use with virtual machines etc
- VMs
  - To store our virtual machine data, such as virtual HDD's if using `KVM`
- Docker
  - To store our `Docker compose` scripts which we will use later on

***

## Enable SMB

Now we need to enable `SMB/CIFS` ready for use on our LAN by going to the `Services` > `SMB/CIFS` > `Settings` page, then clicking the `Enabled` checkbox ![enabled-checkbox-icon](./../images/enabled-checkbox-icon.png) and then clicking the `Save` button ![save-button-icon](./../images/save-button-icon.png).

**Note** If using your own `domain or domain controller` on your LAN, then you'll need to update the `Workgroup` field to contain your `domain name`

All other settings can be ignored for now.

Next head other to the `Services` > `SMB/CIFS` > `Shares` page, to create our `SMB shares`.

![smb-shares](./../images/smb-shares-page.png)

Click the `Create` button ![create-shared-folder-icon](./../images/create-shared-folder-icon.png), and then click the `Shared folder` dropdown to select the first share you would like to configure.

![create-smb-share](./../images/create-smb-share.png)

Ensure that `Browseable` is ticked if this is a publicly shared folder on your LAN, such as the `Media` folder, as well as changing the `Public` dropdown to `Guests allowed`.

As the `Media` share is only used for serving up our media files to our local network and to Plex, we can allow guest access to make it easier to access the media files on other LAN devices, such as watching a movie on an Xbox.

**Note** Obviously, if you are on a `public network` or have a very busy LAN with `alot of Users`, you would only want these folders to be accessible to those whom need it. You should also change the `permissions` to make sure that only those who need to, can write to certain folders.

For example you don't need `Doris` uploading `malware` to the `Movies folder`, ready to be served up by Plex etc for everyone else to access.

With certain folders such as the main `Media\Movies` folder, you could probably set this to `read-only (RO)` for most Users instead of `read-write (RW)`.
Users only need to be able to click through the folders to find the film they need (done by making them `Browseable`), they won't neccesarily need to upload or `write` to the folder.

**Note** Some folders may need `execute` permissions, which are used to launch programs etc. However these folders can still be `read-only` (or more accurately `read-execute`), so that Users can launch apps and still not be able `write` to the folder.

Once ready, click the Save button ![save-button-icon](./../images/save-button-icon.png) to save the `SMB share`.

Repeat this step for any other `SMB shares` you need, such as the `Docker` and `ISOs` shares.

When done creating all of your `shares`, remember to click the yellow `confirm pending changes` prompt at the top of the screen to confirm your changes.

![confirm-pending-changes](./../images/confirm-pending-changes.png)
