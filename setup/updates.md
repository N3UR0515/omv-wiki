# Updates

The next step is to configure OMV to also include `Community-maintened updates`, so that we get access to the additional features like Docker, that we'll be using later on.

To do this you need to navigate to `System` > `Update Management` > `Settings` option on the left.

![community-updates](./../images/community-updates.png)

Once here you'll need to tick the box for `Community-maintained updates` as shown below, then click save.

![community-updates-ticked](./../images/community-updates-ticked.png)

You'll get a prompt informing you that your package list info is out of date. Simply click `Yes` to have it update your package repositories.

![package-reload-prompt](./../images/package-reload-prompt.png)

You should then be redirected to the Updates screen, shown below. From here click the `Install Updates` button ![install-updates-icon](./../images/install-updates-icon.png) located at the top left, under the `navigation menu`.

When prompted, tick the `Confirm` box and then click `Yes` to install the currently available updates.

![install-updates-prompt](./../images/install-updates-prompt.png)

After the updates have completed, click the close button on the updates popup.

![updates-completed](./../images/updates-completed.png)

You should now be greeted with the yellow `confirm pending changes` prompt for the first time at the top of the screen, as shown below.

![confirm-pending-changes](./../images/confirm-pending-changes.png)

You will see this prompt alot! Basically anytime you make changes to OMV and click save, this prompt will appear.

**Note:** The changes you have made to OMV will **NOT** apply until **AFTER** you have clicked the `tick` button in the top right.

Next up we'll add the additional packages and services that we would like to use.
