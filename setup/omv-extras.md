# OMV-Extras

Next we are going to install [OMV-Extras](https://wiki.omv-extras.org/), which allows us to add additional packages and services (such as `Docker`) for use within OpenMediaVault.

This process is pretty straight forward, we just need to run a one-line command on the OMV server via `SSH`.

In order to achieve this though, you need to run the command as a User that is in the `sudo` group (For example the `root` account you created when installing OMV, or a User you created after, such as `doris`, that you added into the `sudo group`).

**Note:** You will also need the User to be in the `ssh` group if you plan to run this command via `SSH`, as we will be doing.

## Installing `SSH`

You will need a couple of things to be able to connect to OMV using `SSH`.

First you'll need a **terminal** of some-kind. I strongly recommend **[Windows Terminal](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=en-gb&gl=GB)**, this is what I currently use.

You can of course just use the **Command Prompt**, or even **PowerShell** if you prefer. You could also use **[PuTTY](https://www.putty.org/)**, which is an `SSH Terminal GUI`.

**Note:** [Windows Terminal](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=en-gb&gl=GB) enables you to combine multiple `shells/terminals` into one place (such as git-bash, CMD, PowerShell, Python etc), with easy access to them all. It is also customisable, which is good if you are like me and don't like the standard `CMD prompt` look and feel.

Obviously, you will also need to have installed `SSH` onto your machine to be able to use it. This is easy to do for most Operating Systems. For example on `Windows` you can achieve this by going to `Settings` > `Apps` > `Optional features` and installing the `OpenSSH Client` feature. (May differ on different versions of Windows)

![windows-install-openssh](./../images/windows-install-openssh.png)

You can do this on **Ubuntu** or **Debian** by running the below command from the **terminal**.

```bash
sudo apt update && sudo apt install ssh -y
```

## Installing OMV-Extras

Once you have installed `SSH` and have it available from a **terminal**, connect to your server's `IP address` using the below command:

```bash
ssh root@192.168.1.1
```

Replacing `root` with your username, and `192.168.1.1` with your OpenMediaVault server's `IP address`. You can also use your server's `hostname`. For example;

```bash
ssh doris@omv.local
```

You'll likely get a few prompts appear asking if you want to trust the connection etc. Navigate through these and then enter your password.

**Note:** *Remember, on Linux your password or any symbols are not displayed when you enter them!*

Once logged in you will be greeted with the **terminal** session as shown below.

![ssh-connection](./../images/ssh-connection.png)

From here, all you need to do is run the following command with a User who has `sudo` rights:

```bash
wget -O - https://github.com/OpenMediaVault-Plugin-Developers/packages/raw/master/install | sudo bash
```

After this completes, you *should* now have **OMV-Extras** installed and available within **OMV**!

![omv-extras-available](./../images/omv-extras-available.png)

## Configure OMV-Extras

Now that **OMV-Extras** is available in your `management web page` you'll need to go to `System` > `omv-extras` > `Settings` and enable `Backports` and click `Save` ![save-button-icon](./../images/save-button-icon.png), if not already enabled.

![backports-enabled](./../images/backports-enabled.png)

You'll probably be prompted to update your `packages` now, if it hasn't already asked you to.

## Install Docker

With **OMV-Extras** now installed and updated, we can go ahead and install **Docker**.

To do this, go to `System` > `omv-extras` > `Docker` and click the `Install` button ![install-button](./../images/install-button.png)

![docker-installed](./../images/docker-installed.png)

You can also install **[Portainer](https://www.portainer.io/)** using the same method, however this is optional. **[Portainer](https://www.portainer.io/)** is an easy to use web GUI for managing **Docker** environments. Give it a try!

**Note:** *Remember to click the yellow `confirm pending changes` prompt at the top of the screen to confirm any changes you make, or they will not be applied!*

![confirm-pending-changes](./../images/confirm-pending-changes.png)

## Additional Plugins

Once you have installed **[OMV-Extras](./omv-extras.md)**, be sure to go back and install any other packages you need from the `Plugins` menu in the `web management page`.

I recommend installing these `plugins`:

- `openmediavault-apttool`
  - Allows managing your `apt repositories`
- `openmediavault-compose`
  - For use with `Docker` later
- `openmediavault-fail2ban`
  - Protects against suspicious logins
- `openmediavault-kvm`
  - If you want to run `virtual machines` on your server
- `openmediavault-minidlna`
  - For use with serving media files through `Plex` and over the `LAN`
- `openmediavault-remotemount`
  - If you want to mount external filesystems to this server, such as `NAS` share
  - `openmediavault-wetty`
    - If you want to be able to connect to your `OMV` server's terminal in the `management web page`
