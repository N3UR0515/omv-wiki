# Media Server

Automated media server setup based on Docker using a reverse proxy for secure connections.

## Containers

- [rTorrent](#rtorrent)

- [Jackett](#jackett)

- [Sonarr](#sonarr)

- [Radarr](#radarr)

- [Ombi](#ombi)

- [Tautulli](#tautulli)

- [Plex](#plex)

## VPN

You need to have already purchased a VPN service through a provider that offers OpenVPN connections, such as [PIA](https://www.privateinternetaccess.com/), [NordVPN](https://nordvpn.com/), [CyberGhost](https://www.cyberghostvpn.com/) etc.

I currently use [CyberGhost](https://www.cyberghostvpn.com/).

**Note:** You can use WireGuard instead of OpenVPN, however I will not be covering that in this guide just yet.

Once you have purchased your VPN service, download the `OpenVPN Connection` files from your VPN provider. Extract the files if needed and copy them to the `rtorrent\openvpn` folder, as shown in the image below.

![rTorrent Files](./../images/vpnfiles.png)

## Preparation

You will need to install `Git` before continuing:

`sudo apt update && sudo apt install git`

Next use `Git` to clone the media server repo into your home directory:

`cd ~/ && git clone https://github.com/NEUR0515/NeuroVision.git`

----

## Constants

All constants (Passwords, Usernames, API Keys etc.) will be defined in the `NeuroVision/.env` file.

Rename the included `.env-example` to `.env` and update the info and passwords as needed.

----

## Networking

**Note:** If you do not use the same network name as defined here, be sure to update your `.env` file with the correct name

Create `mediaNet` network for Media containers

    docker network create mediaNet

----

## rTorrent

Arch Linux running rTorrent with ruTorrent webui and OpenVPN by [binhex](https://github.com/binhex) on [DockerHub](https://hub.docker.com/r/binhex/arch-rtorrentvpn).

**Note:** This container is used to download the media files through a VPN connection

### rTorrent Ports

    - 9080
    - 9443
    - 8118
    - 49184
    - 6881
    - 5000

----

## Jackett

Jackett container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/jackett). Connects to the `rTorrent` Privoxy service (port `8118`) to search for torrents via the VPN (configured via the Jackett webUI Proxy settings).

**Note:** This container is used to search for the media files for the `Sonarr` and `Radarr` containers

### Jackett Ports

    - 9117

----

## Sonarr

Sonarr container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/sonarr)

**Note:** This container is used to manage TV shows

### Sonarr Ports

    - 8989

----

## Radarr

Radarr container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/radarr)

**Note:** This container is used to manage movies

### Radarr Ports

    - 7878

----

## Ombi

Ombi container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/ombi)

**Note:** This container adds an easy to use interface (with optional mobile apps availble) to search for and add TV shows and movies

### Ombi Ports

    - 3579

----

## Tautulli

Tautulli container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/tautulli)

**Note:** This container is used to manage the Plex media server below

### Tautulli Ports

    - 8181

----

## Plex

Plex container by [linuxserver.io](https://www.linuxserver.io/) on [DockerHub](https://hub.docker.com/r/linuxserver/plex)

**Note:** This container is used for the main media server that hosts your media files, and is available on your LAN

### Plex Ports

    - 32400

The following are the recommended parameters. Each of the following parameters to the container are treated as first-run parameters only. That is, all other parameters are ignored on subsequent runs of the server. We recommend that you set the following parameters:

- HOSTNAME
  - Sets the hostname inside the docker container. For example -h PlexServer will set the servername to PlexServer. Not needed in Host Networking.

- TZ
  - Set the timezone inside the container. For example: Europe/London. The complete list can be [found here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).

- PLEX_CLAIM
  - The claim token used to connect the media server to your Plex account. If not provided, the server will not be automatically logged in. If server is already logged in, this parameter is ignored. You can use [this link](https://www.plex.tv/claim) to obtain a claim token for your Plex server.

----
